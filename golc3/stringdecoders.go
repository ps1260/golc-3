// stringdecoders
package main

import (
	"fmt"
	"strconv"
	"strings"
)

//convert "R0", "R1", ... to the enum for registers
func reg_decode(err *bool, line_num int, in string) (out Register) {
	switch strings.ToUpper(in) {
	case "R0":
		out = R0
	case "R1":
		out = R1
	case "R2":
		out = R2
	case "R3":
		out = R3
	case "R4":
		out = R4
	case "R5":
		out = R5
	case "R6":
		out = R6
	case "R7":
		out = R7
	default:
		*err = true
		fmt.Println(line_num, "- Error: Unable to decode register.")
	}
	return
}

//takes a string for some number offset, and convert it to a number
func offset_decode(err *bool, line_num int, in string) int16 {
	in2 := ""
	base := 16
	if in[0] == '#' {
		in2 = in[1:]
		base = 10
	} else if in[0] == 'X' || in[0] == 'x' {
		in2 = in[1:]
	} else if len(in) > 2 && (in[:2] == "0x" || in[:2] == "0X") {
		in2 = in[2:]
	} else {
		in2 = in
	}
	num, temp_err := strconv.ParseInt(in2, base, 16)
	if temp_err != nil {
		*err = true
		fmt.Println(line_num, "- Error: Unable to decode offset.")
	}
	return int16(num)
}

//takes a string for some number offset, and convert it to a number
func uoffset_decode(err *bool, line_num int, in string) uint16 {
	in2 := ""
	base := 16
	if in[0] == '#' {
		in2 = in[1:]
		base = 10
	} else if in[0] == 'X' || in[0] == 'x' {
		in2 = in[1:]
	} else if len(in) > 2 && (in[:2] == "0x" || in[:2] == "0X") {
		in2 = in[2:]
	} else {
		in2 = in
	}
	num, temp_err := strconv.ParseUint(in2, base, 16)
	if temp_err != nil {
		*err = true
		fmt.Println(line_num, "- Error: Unable to decode address.")
	}
	return uint16(num)
}

//May be given a label. If so, return errx=true
func data_decode(in string, force_hex bool) (out []uint16, errx bool) {
	errx = false
	if force_hex {
		num, err := strconv.ParseUint("0x"+in, 0, 16)
		panic_on_error(err)
		out = append(out, uint16(num))
	} else if in[0] == '#' {
		num, err := strconv.ParseUint(in[1:], 0, 16)
		panic_on_error(err)
		out = append(out, uint16(num))
	} else if in[0] == 'X' || in[0] == 'x' {
		num, err := strconv.ParseUint("0"+in, 0, 16)
		panic_on_error(err)
		out = append(out, uint16(num))
	} else if in[:1] == "0X" || in[:1] == "0x" {
		num, err := strconv.ParseUint(in, 0, 16)
		panic_on_error(err)
		out = append(out, uint16(num))
	} else if in[0] == '\'' {
		out = append(out, uint16(in[1]))
	} else {
		errx = true
	}
	return
}

func remove_empty(in []string) (out []string) {
	for i := range in {
		if in[i] != "" {
			out = append(out, in[i])
		}
	}
	return
}

func register_print(in Register) (out string) {
	switch in {
	case R0:
		out = "R0"
	case R1:
		out = "R1"
	case R2:
		out = "R2"
	case R3:
		out = "R3"
	case R4:
		out = "R4"
	case R5:
		out = "R5"
	case R6:
		out = "R6"
	case R7:
		out = "R7"
	}
	return
}

func num_print(in int16) string {
	return "#" + strconv.Itoa(int(in))
}

func unum_print(in uint16) string {
	return "#" + strconv.Itoa(int(in))
}

func hex_print(in int16) string {
	return fmt.Sprintf("x%4.4X", in)
}

func uhex_print(in uint16) string {
	return fmt.Sprintf("x%4.4X", in)
}

func address_print(in uint16) string {
	return fmt.Sprintf("x%4.4X", in)
}

func get_string(line string) string {
	esc_seq := 0
	s_idx := strings.IndexByte(line, '"') + 1
	var i int
	for i = s_idx; i < len(line) && (line[i] != '"' || esc_seq%2 == 1); i++ {
		if line[i] == '\\' {
			esc_seq++
		} else {
			esc_seq = 0
		}
	}
	return line[s_idx:i]
}
