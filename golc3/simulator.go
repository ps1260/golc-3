// simulator
package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

func simulate(memory [0xffff + 1]uint16, starting_address uint16, verbose bool, sym_table map[uint16]string) {
	//Infinite loop that evaluates an insturction, prints out LC-3 status after
	//cycle, and then waits .5 seconds til next cycle
	var state lc3_system_state
	state.pc = starting_address
	state.memory = memory
	for {
		state.evaluate_instruction(verbose, sym_table)
		if verbose {
			fmt.Printf("\tPC: 0x%4.4X IR: 0x%4.4X NZP: %d%d%d\n", state.pc, state.ir, state.n, state.z, state.p)
			fmt.Printf("Registers: 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X\n\n",
				state.regfile[0], state.regfile[1], state.regfile[2], state.regfile[3],
				state.regfile[4], state.regfile[5], state.regfile[6], state.regfile[7])
			time.Sleep(500 * time.Millisecond)
		}

		//Detect some special end of program cases to cease operation in a nice way
		if state.ir == 0xF025 {
			fmt.Println("Detecting a HALT instruction. Goodbye...")
			return
		} else if bit(state.memory[0xfffe], 15) == 1 {
			fmt.Println("The Machine control register has triggered a poweroff. Goodbye...")
			return
		}
	}
}

//Evalutes the instruction at the given location PC. Based on the flow chart in
//the LC-3 book
func (state *lc3_system_state) evaluate_instruction(verbose bool, sym_table map[uint16]string) {
	state.mar = state.pc
	state.pc++
	if state.interupt {
		state.interupt_sequence()
	} else {
		state.memread()
		//state.mdr = state.memory[state.mar]
		state.ir = state.mdr
		state.ben = (state.n == bit(state.ir, 11)) || (state.z == bit(state.ir, 10)) || (state.p == bit(state.ir, 9))
		if verbose {
			state.decode_ir(sym_table)
		}
		switch state.ir >> 12 {
		case 0x1:
			state.add()
		case 0x5:
			state.and()
		case 0x0:
			state.br()
		case 0xc:
			state.jmp()
		case 0x4:
			state.jsr()
		case 0x2:
			state.ld()
		case 0xa:
			state.ldi()
		case 0x6:
			state.ldr()
		case 0xe:
			state.lea()
		case 0x9:
			state.not()
		case 0x8:
			state.rti()
		case 0x3:
			state.st()
		case 0xb:
			state.sti()
		case 0x7:
			state.str()
		case 0xf:
			state.trap()
		case 0xd:
			state.undefined_opcode()
		default:
			panic("undefined instruction")
		}
	}
}

//For debugging, decode the current ir as assembly
func (state *lc3_system_state) decode_ir(sym_table map[uint16]string) {
	switch state.ir >> 12 {
	case 0x1:
		fmt.Print("ADD ")
		if bit(state.ir, 5) == 1 {
			fmt.Printf("R%d, R%d, #%d\n", state.dr(), state.sr1(), ustos(sext(state.imm5(), 5)))
		} else {
			fmt.Printf("R%d, R%d, R%d\n", state.dr(), state.sr1(), state.sr2())
		}
	case 0x5:
		fmt.Print("AND ")
		if bit(state.ir, 5) == 1 {
			fmt.Printf("R%d, R%d, #%d\n", state.dr(), state.sr1(), ustos(sext(state.imm5(), 5)))
		} else {
			fmt.Printf("R%d, R%d, R%d\n", state.dr(), state.sr1(), state.sr2())
		}
	case 0x0:
		fmt.Print("BR")
		switch state.dr() {
		case 1:
			fmt.Print("p")
		case 2:
			fmt.Print("z")
		case 3:
			fmt.Print("zp")
		case 4:
			fmt.Print("n")
		case 5:
			fmt.Print("np")
		case 6:
			fmt.Print("nz")
		case 7:
			fmt.Print("nzp")
		default:
		}
		if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
			fmt.Printf(" 0x%4.4x\n", state.pc+sext(state.pcoffset9(), 9))
		} else {
			fmt.Printf(" %s\n", sym_table[state.pc+sext(state.pcoffset9(), 9)])
		}
	case 0xc:
		if state.baser() == 7 {
			fmt.Printf("RET\n")
		} else {
			fmt.Printf("JMP R%d\n", state.dr())
		}
	case 0x4:
		if state.ir&0x0800 > 0 {
			fmt.Print("JSR ")
			if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
				fmt.Printf("0x%4.4x\n", state.pc+sext(state.pcoffset9(), 9))
			} else {
				fmt.Printf("%s\n", sym_table[state.pc+sext(state.pcoffset9(), 9)])
			}
		} else {
			fmt.Printf("JSRR R%d\n", state.baser())
		}
	case 0x2:
		if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
			fmt.Printf("LD R%d, 0x%4.4x\n", state.dr(), state.pc+sext(state.pcoffset9(), 9))
		} else {
			fmt.Printf("LD R%d, %s\n", state.dr(), sym_table[state.pc+sext(state.pcoffset9(), 9)])
		}
	case 0xa:
		if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
			fmt.Printf("LDI R%d, 0x%4.4x\n", state.dr(), state.pc+sext(state.pcoffset9(), 9))
		} else {
			fmt.Printf("LDI R%d, %s\n", state.dr(), sym_table[state.pc+sext(state.pcoffset9(), 9)])
		}
	case 0x6:
		fmt.Printf("LDR R%d, R%d, %4.4x\n", state.dr(), state.baser(), state.offset6())
	case 0xe:
		if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
			fmt.Printf("LEA R%d, 0x%4.4x\n", state.dr(), state.pc+sext(state.pcoffset9(), 9))
		} else {
			fmt.Printf("LEA R%d, %s\n", state.dr(), sym_table[state.pc+sext(state.pcoffset9(), 9)])
		}
	case 0x9:
		fmt.Printf("NOT R%d, R%d\n", state.dr(), state.sr1())
	case 0x8:
		fmt.Print("RTI\n")
	case 0x3:
		if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
			fmt.Printf("ST R%d, 0x%4.4x\n", state.dr(), state.pc+sext(state.pcoffset9(), 9))
		} else {
			fmt.Printf("ST R%d, %s\n", state.dr(), sym_table[state.pc+sext(state.pcoffset9(), 9)])
		}
	case 0xb:
		if sym_table[state.pc+sext(state.pcoffset9(), 9)] == "" {
			fmt.Printf("STI R%d, 0x%4.4x\n", state.dr(), state.pc+sext(state.pcoffset9(), 9))
		} else {
			fmt.Printf("STI R%d, %s\n", state.dr(), sym_table[state.pc+sext(state.pcoffset9(), 9)])
		}
	case 0x7:
		fmt.Printf("STR R%d, R%d, %4.4x\n", state.dr(), state.baser(), state.offset6())
	case 0xf:
		switch state.trapvect8() {
		case 0x20:
			fmt.Print("GETC\n")
		case 0x21:
			fmt.Print("OUT\n")
		case 0x22:
			fmt.Print("PUTS\n")
		case 0x23:
			fmt.Print("IN\n")
		case 0x24:
			fmt.Print("PUTSP\n")
		case 0x25:
			fmt.Print("HALT\n")
		default:
			fmt.Printf("TRAP %2.2x\n", state.trapvect8())
		}
	case 0xd:
		fmt.Printf("DATA: %4.4x\n", state.ir)
		return
	default:
		panic("undefined instruction")
	}
}

//unsigned to signed
func ustos(num uint16) int16 {
	if bit(num, 15) == 1 {
		return -1 - int16(^num)
	} else {
		return int16(num)
	}
}

//Returns the i-th bit of num as a boole
func bit(num uint16, i uint16) uint16 {
	return (num & (1 << i)) >> i
}

//sign extends the number num, where bitlen is the size of unextended
//number
func sext(num, bitlen uint16) uint16 {
	if bit(num, bitlen-1) == 1 {
		return num + 0xffff<<bitlen
	} else {
		return num
	}
}

//sets the conditional codes n,z,p based on the sign of the current destination
//register
func (state *lc3_system_state) setcc() {
	if state.regfile[state.dr()] == 0 {
		state.n = 0
		state.z = 1
		state.p = 0
	} else if bit(state.regfile[state.dr()], 15) == 1 {
		state.n = 1
		state.z = 0
		state.p = 0
	} else {
		state.n = 0
		state.z = 0
		state.p = 1
	}
}

/*
The next functions read the various parts of the instruction register, and
return them as uint16
*/

//gives the value of the base register
func (state *lc3_system_state) baser() uint16 {
	return (state.ir & 0x01c0) >> 6
}

//gives the value of the destination register
func (state *lc3_system_state) dr() uint16 {
	return (state.ir & 0x0e00) >> 9
}

//gives the source register (#1) of the current instruction
func (state *lc3_system_state) sr1() uint16 {
	return (state.ir & 0x01c0) >> 6
}

//gives the source register (#2) of the current instruction
func (state *lc3_system_state) sr2() uint16 {
	return (state.ir & 0x0007)
}

//gives the immediate 5 bit value
func (state *lc3_system_state) imm5() uint16 {
	return (state.ir & 0x001f)
}

//gives the 6 bit offset value
func (state *lc3_system_state) offset6() uint16 {
	return (state.ir & 0x003f)
}

//gives the 9 bit pc offset
func (state *lc3_system_state) pcoffset9() uint16 {
	return (state.ir & 0x01ff)
}

//gives the 11 bit pc offset
func (state *lc3_system_state) pcoffset11() uint16 {
	return (state.ir & 0x07ff)
}

//gives the 8 bit trap vector
func (state *lc3_system_state) trapvect8() uint16 {
	return (state.ir & 0x00ff)
}

//these read/write functions allow for memory mapping user i/o
func (state *lc3_system_state) memread() {
	switch state.mar {
	case 0xfe02:
		//gets input from the user. Not ideal, since it requires an enter to trigger
		r := bufio.NewReader(os.Stdin)
		kbdr, err := r.ReadByte()
		if err != nil {
			panic(err)
		}
		state.mdr = uint16(kbdr)
	case 0xfe00:
		//kbsr is always triggered (kbsr[15]=1) since the read will wait until user
		//input. If a better method is found, then this should be changed
		state.kbsr = 0x8000
		state.mdr = state.kbsr
	case 0xfe04:
		//dsr is always triggered (dsr[15]=1) since the display here is always
		//ready to print
		state.dsr = 0x8000
		state.mdr = state.dsr
	default:
		state.mdr = state.memory[state.mar]
	}
}

func (state *lc3_system_state) memwrite() {
	//fmt.Println("Woot, wrote ", state.mdr, " to ", state.mar)
	if state.mar == 0xfe06 {
		state.ddr = state.mdr
		fmt.Printf("%c", uint8(state.ddr))
	} else {
		state.memory[state.mar] = state.mdr
	}
}

/*
Next up we have the various opcode. These are based on the flow chart from the book.
Each one corresponds to a single opcode, and if the opcode coresponds to multiple
instructions, it will interpret it accordingly
*/

func (state *lc3_system_state) add() {
	if bit(state.ir, 5) == 1 {
		state.regfile[state.dr()] = state.regfile[state.sr1()] + sext(state.imm5(), 5)
	} else {
		state.regfile[state.dr()] = state.regfile[state.sr1()] + state.regfile[state.sr2()]
	}
	state.setcc()
}
func (state *lc3_system_state) and() {
	if bit(state.ir, 5) == 1 {
		state.regfile[state.dr()] = state.regfile[state.sr1()] & sext(state.imm5(), 5)
	} else {
		state.regfile[state.dr()] = state.regfile[state.sr1()] & state.regfile[state.sr2()]
	}
	state.setcc()
}
func (state *lc3_system_state) br() {
	if state.ben {
		state.pc = state.pc + sext(state.pcoffset9(), 9)
	}
}
func (state *lc3_system_state) jmp() {
	state.pc = state.regfile[state.baser()]
}
func (state *lc3_system_state) jsr() {
	state.regfile[7] = state.pc
	if bit(state.ir, 11) == 1 {
		state.pc = state.pc + sext(state.pcoffset11(), 11)
	} else {
		state.pc = state.regfile[state.baser()]
	}
}
func (state *lc3_system_state) ld() {
	state.mar = state.pc + sext(state.pcoffset9(), 9)
	state.memread()
	state.regfile[state.dr()] = state.mdr
	state.setcc()
}
func (state *lc3_system_state) ldi() {
	state.mar = state.pc + sext(state.pcoffset9(), 9)
	state.memread()
	state.mar = state.mdr
	state.memread()
	state.regfile[state.dr()] = state.mdr
	state.setcc()
}
func (state *lc3_system_state) ldr() {
	state.mar = state.regfile[state.baser()] + sext(state.offset6(), 6)
	state.memread()
	state.regfile[state.dr()] = state.mdr
	state.setcc()
}
func (state *lc3_system_state) lea() {
	state.regfile[state.dr()] = state.pc + sext(state.pcoffset9(), 9)
	state.setcc()
}
func (state *lc3_system_state) not() {
	state.regfile[state.dr()] = ^state.sr1()
	state.setcc()
}
func (state *lc3_system_state) rti() {
	panic("ERROR: Attempt to execute RTI. This is unknown/unfinished territory. Exiting...")
	state.mar = state.sp
	if bit(state.psr, 15) == 1 {
		state.vector = 0x00
		state.mdr = state.psr
		state.psr = state.psr & 0x7fff
		state.line_45()
	} else {
		state.memread()
		state.pc = state.mdr
		state.mar = state.sp + 1
		state.sp++
		state.memread()
		state.psr = state.mdr
		state.sp++
		if bit(state.psr, 15) == 1 {
			state.saved_ssp = state.sp
			state.sp = state.saved_usp
		}
	}
}
func (state *lc3_system_state) st() {
	state.mar = state.pc + sext(state.pcoffset9(), 9)
	state.mdr = state.regfile[state.sr1()]
	state.memwrite()
	//state.memory[state.mar] = state.mdr
}
func (state *lc3_system_state) sti() {
	state.mar = state.pc + sext(state.pcoffset9(), 9)
	state.memread()
	state.mar = state.mdr
	state.mdr = state.regfile[state.sr1()]
	state.memwrite()
	//state.memory[state.mar] = state.mdr
}
func (state *lc3_system_state) str() {
	state.mar = state.regfile[state.baser()] + sext(state.offset6(), 6)
	state.mdr = state.regfile[state.dr()]
	state.memwrite()
	//state.memory[state.mar] = state.mdr
}
func (state *lc3_system_state) trap() {
	state.mar = state.ir & 0x00ff
	state.memread()
	state.regfile[7] = state.pc
	state.pc = state.mdr
}
func (state *lc3_system_state) undefined_opcode() {
	panic("ERROR: Attempted to execute undefined opcode. Exiting...")
	state.vector = 0x01
	state.mdr = state.psr
	state.psr = state.psr & 0x7fff
	if bit(state.psr, 15) == 1 {
		state.line_45()
	} else {
		state.line_37()
	}
}
func (state *lc3_system_state) line_45() {
	state.saved_usp = state.sp
	state.sp = state.saved_ssp
	state.line_37()
}
func (state *lc3_system_state) line_37() {
	state.mar = state.sp - 1
	state.sp = state.sp - 1
	state.memwrite()
	state.mdr = state.pc + 1
	state.mar = state.sp - 1
	state.sp = state.sp - 1
	state.memwrite()
	state.mar = 0x0100 + uint16(state.vector)
	state.memread()
	state.pc = state.mdr
}

//This is wonky, not fully implemented stuff
func (state *lc3_system_state) interupt_sequence() {
	state.vector = state.intv
	state.psr = state.psr & (0xf8ff + state.priority<<8)
	state.mdr = state.psr
	state.psr = state.psr & 0x7fff
	if bit(state.psr, 15) == 1 {
		state.line_45()
	} else {
		state.line_37()
	}
}
