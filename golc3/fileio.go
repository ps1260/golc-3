// fileio
package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"
)

func load_file_strarr(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		panic("Bad filename give")
	}
	defer file.Close()
	var sourcecode []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sourcecode = append(sourcecode, scanner.Text())
	}
	return sourcecode
}

//first 2 bytes of the .obj file should be the address where the code starts
func write_obj_file(filename string, data []uint16, address uint16) {
	bytearr := make([]byte, 2*len(data)+2)
	bytearr[0], bytearr[1] = byte(address>>8), byte(address&0x00ff)
	for i := range data {
		bytearr[2*i+2], bytearr[2*i+3] = byte(data[i]>>8), byte(data[i]&0x00ff)
	}
	err := ioutil.WriteFile(filename+".obj", bytearr, 0644)
	panic_on_error(err)
	return
}

//Load an obj file into the LC-3 memory
func load_obj_file(filename string, memory *[0xffff + 1]uint16) (starting_address uint16) {
	filecontents, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("ERROR: Error reading " + filename)
		return
	}
	//We read the file as bytes, but LC-3 is 16 bit, so we must join each pair of
	//bytes together
	//The first 16 bits of the file are the starting address of the contents
	address := uint16(filecontents[0])<<8 + uint16(filecontents[1])
	//sets the starting PC to the begining of the file contents
	starting_address = address
	for i := 2; i < len(filecontents); i += 2 {
		memory[address] = uint16(filecontents[i])<<8 + uint16(filecontents[i+1])
		address++
	}
	return
}

//Load an obj file as an array of instructions/data
func load_obj_file_as_binarr(filename string) (binarr []uint16, starting_address uint16) {
	filecontents, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("bad filename")
	}
	//We read the file as bytes, but LC-3 is 16 bit, so we must join each pair of
	//bytes together
	//The first 16 bits of the file are the starting address of the contents
	address := uint16(filecontents[0])<<8 + uint16(filecontents[1])
	//sets the starting PC to the begining of the file contents
	starting_address = address
	for i := 2; i < len(filecontents); i += 2 {
		binarr = append(binarr, uint16(filecontents[i])<<8+uint16(filecontents[i+1]))
		address++
	}
	return
}

func write_file_strarr(filename string, data []string) {
	file, err := os.Create(filename)
	panic_on_error(err)
	defer file.Close()
	writer := bufio.NewWriter(file)
	for i := range data {
		fmt.Fprintln(writer, data[i])
	}
	writer.Flush()
	return
}

//Creates the .sym file that the original lc3 assembler makes. This should
//format it in the same way.
func sym_file(inmap map[string]uint16, startaddress uint16, filename string) {
	var out []string
	out = append(out, "// Symbol table")
	out = append(out, "// Scope level 0:")
	out = append(out, "//\tSymbol Name       Page Address")
	out = append(out, "//\t----------------  ------------")
	//Make an inverse map (address->label)
	inv_map := make(map[uint16]string)
	for key, value := range inmap {
		inv_map[value] = key
	}
	//Make a list of addresses and order them
	var keys []int
	for k := range inv_map {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)
	//Print the labels and addresses in order of address
	for _, key := range keys {
		str := "//\t" + inv_map[uint16(key)]
		for i := 0; i < 18-len(inv_map[uint16(key)]); i++ {
			str += " "
		}
		out = append(out, str+fmt.Sprintf("%4.4X", key))
	}
	out = append(out, "")
	write_file_strarr(filename, out)
}

func load_symbol_file(symboltable map[uint16]string, filename string) {
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("WARNING: Couldn't find symbol file ", filename)
		return
	}
	defer file.Close()
	var filecontents []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		filecontents = append(filecontents, scanner.Text())
	}
	var line []string
	for i := 4; i < len(filecontents)-1; i++ {
		line = remove_empty(strings.Split(filecontents[i][3:], " "))
		tmp, _ := data_decode(line[1], true)
		symboltable[tmp[0]] = line[0]
	}
}
