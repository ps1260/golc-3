// lc3formatter
package main

import (
	"fmt"
	"strconv"
)

const CODE_INDENT = 16
const COMMENT_INDENT = 36
const MAX_LINE_LENGTH = 80

func format_instruction(in instruction, line_num int) (out string) {
	for i := 0; i < len(in.labels); i++ {
		out += in.labels[i] + " "
	}
	if len(out) >= CODE_INDENT {
		fmt.Println(line_num, "- Warning: Labels exceeds", CODE_INDENT, "characters. Please consider shorter labels, or splitting them accross lines.")
	} else {
		temp_padding := CODE_INDENT - len(out)
		for i := 0; i < temp_padding; i++ {
			out += " "
		}
	}
	switch in.opp {
	case ORIG:
		out += ".ORIG " + address_print(in.unum)
	case END:
		out += ".END"
	case ADD:
		if in.form == 0 {
			out += "ADD " + register_print(in.reg[0]) + ", " + register_print(in.reg[1]) + ", " + register_print(in.reg[2])
		} else {
			out += "ADD " + register_print(in.reg[0]) + ", " + register_print(in.reg[1]) + ", " + num_print(in.num)
		}
	case AND:
		if in.form == 0 {
			out += "AND " + register_print(in.reg[0]) + ", " + register_print(in.reg[1]) + ", " + register_print(in.reg[2])
		} else {
			out += "AND " + register_print(in.reg[0]) + ", " + register_print(in.reg[1]) + ", " + num_print(in.num)
		}
	case BR:
		out += "BR"
		switch in.form {
		case 0:
			out += " "
		case 1:
			out += "p "
		case 2:
			out += "z "
		case 3:
			out += "zp "
		case 4:
			out += "n "
		case 5:
			out += "np "
		case 6:
			out += "nz "
		case 7:
			out += "nzp "
		}
		if in.labeloffset != "" {
			out += in.labeloffset
		} else {
			out += num_print(in.num)
			fmt.Println(line_num, "- Warning: Detected branch with explicit offset. Please use a label.")
		}
	case JMP:
		out += "JMP " + register_print(in.reg[0])
	case JSR:
		out += "JSR " + num_print(in.num)
	case JSRR:
		out += "JSRR " + register_print(in.reg[0])
	case LD:
		if in.form == LABEL {
			out += "LD " + register_print(in.reg[0]) + ", " + in.labeloffset
		} else {
			out += "LD " + register_print(in.reg[0]) + ", " + hex_print(in.num)
		}
	case LDI:
		if in.form == LABEL {
			out += "LDI " + register_print(in.reg[0]) + ", " + in.labeloffset
		} else {
			out += "LDI " + register_print(in.reg[0]) + ", " + hex_print(in.num)
		}
	case LDR:
		out += "LDR " + register_print(in.reg[0]) + ", " + register_print(in.reg[1]) + ", " + num_print(in.num)
	case LEA:
		if in.form == LABEL {
			out += "LEA " + register_print(in.reg[0]) + ", " + in.labeloffset
		} else {
			out += "LEA " + register_print(in.reg[0]) + ", " + hex_print(in.num)
		}
	case NOT:
		out += "NOT " + register_print(in.reg[0]) + ", " + register_print(in.reg[1])
	case RET:
		out += "RET"
	case RTI:
		out += "RTI"
	case ST:
		if in.form == LABEL {
			out += "ST " + register_print(in.reg[0]) + ", " + in.labeloffset
		} else {
			out += "ST " + register_print(in.reg[0]) + ", " + hex_print(in.num)
		}
	case STI:
		if in.form == LABEL {
			out += "STI " + register_print(in.reg[0]) + ", " + in.labeloffset
		} else {
			out += "STI " + register_print(in.reg[0]) + ", " + hex_print(in.num)
		}
	case STR:
		out += "STR " + register_print(in.reg[0]) + ", " + register_print(in.reg[1]) + ", " + num_print(in.num)
	case TRAP:
		switch in.unum {
		case 0x20:
			out += "GETC"
		case 0x21:
			out += "OUT"
		case 0x22:
			out += "PUTS"
		case 0x23:
			out += "IN"
		case 0x24:
			out += "PUTSP"
		case 0x25:
			out += "HALT"
		default:
			out += "TRAP " + address_print(uint16(in.num))
		}
	case DATA:
		if in.form == BLKW {
			out += ".BLKW " + fmt.Sprintf("%d", len(in.data))
		} else {
			if len(in.data) == 0 {
				if in.labeloffset != "" {
					out += ".FILL " + in.labeloffset
				} else {
					out += ".FILL " + uhex_print(in.unum)
				}
			} else if len(in.data) == 1 {
				out += ".FILL " + address_print(in.data[0])
			} else {
				out += ".STRINGZ \""
				data := []byte{}
				for i := 0; i < len(in.data); i++ {
					data = strconv.AppendQuoteRuneToASCII(data, rune(in.data[i]))
				}
				/*for i := range in.data {
				out += string(byte(in.data[i]))
				>*/
				out += "\""
			}
		}
	default:
	}
	if len(out) >= COMMENT_INDENT {
		fmt.Println(line_num, "- Warning: Code exceeds", COMMENT_INDENT, "characters. Please consider shorter labels, or splitting them accross lines.")
	} else if in.comment != "" {
		temp_padding := COMMENT_INDENT - len(out)
		for i := 0; i < temp_padding; i++ {
			out += " "
		}
		out += "; " + in.comment
	}
	if len(out) > MAX_LINE_LENGTH {
		fmt.Println(line_num, "- Warning: Line exceeds", MAX_LINE_LENGTH, "characters. Please consider shortening it.")
	}
	return
}
