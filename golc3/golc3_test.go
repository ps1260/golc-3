// golc3_test
package main

import (
	"testing"
)

func Test_str2instruction2(t *testing.T) {
	line := "add r1, r1, 4"
	expected := make_inst(ADD, IMMEDIATE, 0, [3]Register{R1, R1, 0}, 4, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "add r0, r1, r2"
	expected = make_inst(ADD, REGISTER, 0, [3]Register{R0, R1, R2}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "AND r0, r1, r2"
	expected = make_inst(AND, REGISTER, 0, [3]Register{R0, R1, R2}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "JMP r5"
	expected = make_inst(JMP, 0, 0, [3]Register{R5, 0, 0}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "JSR LABEL"
	expected = make_inst(JSR, LABEL, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "", "LABEL", []uint16{})
	check_decoder(t, line, expected)

	line = "JSR #6"
	expected = make_inst(JSR, IMMEDIATE, 0, [3]Register{0, 0, 0}, 6, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "JSRR r5"
	expected = make_inst(JSRR, 0, 0, [3]Register{R5, 0, 0}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = ""
	expected = make_inst(NOCODE, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = ";"
	expected = make_inst(NOCODE, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = ";simple comment"
	expected = make_inst(NOCODE, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "simple comment", "", []uint16{})
	check_decoder(t, line, expected)

	line = ";complex, \"comment"
	expected = make_inst(NOCODE, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "complex, \"comment", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LABEL not r1,r6"
	expected = make_inst(NOT, 0, 0, [3]Register{R1, R6, 0}, 0, 0, []string{"LABEL"}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LABEL LD r1,TEST"
	expected = make_inst(LD, LABEL, 0, [3]Register{R1, 0, 0}, 0, 0, []string{"LABEL"}, "", "TEST", []uint16{})
	check_decoder(t, line, expected)

	line = "LABEL LD r1, #5"
	expected = make_inst(LD, IMMEDIATE, 0, [3]Register{R1, 0, 0}, 5, 0, []string{"LABEL"}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LABEL LD r1, #-5"
	expected = make_inst(LD, IMMEDIATE, 0, [3]Register{R1, 0, 0}, -5, 0, []string{"LABEL"}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LD r1, x10"
	expected = make_inst(LD, IMMEDIATE, 0, [3]Register{R1, 0, 0}, 16, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LD r1, 0x10"
	expected = make_inst(LD, IMMEDIATE, 0, [3]Register{R1, 0, 0}, 16, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LD r1, X10"
	expected = make_inst(LD, IMMEDIATE, 0, [3]Register{R1, 0, 0}, 16, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "LD r1, 0X10"
	expected = make_inst(LD, IMMEDIATE, 0, [3]Register{R1, 0, 0}, 16, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BR #-1"
	expected = make_inst(BR, IMMEDIATE, NZP, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRp #-1"
	expected = make_inst(BR, IMMEDIATE, P, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRz #-1"
	expected = make_inst(BR, IMMEDIATE, Z, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRzp #-1"
	expected = make_inst(BR, IMMEDIATE, ZP, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRn #-1"
	expected = make_inst(BR, IMMEDIATE, N, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRnp #-1"
	expected = make_inst(BR, IMMEDIATE, NP, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRnz #-1"
	expected = make_inst(BR, IMMEDIATE, NZ, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "BRnzp #-1"
	expected = make_inst(BR, IMMEDIATE, NZP, [3]Register{0, 0, 0}, -1, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = ".orig x3000"
	expected = make_inst(ORIG, 0, 0, [3]Register{0, 0, 0}, 0, 0x3000, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = ".END"
	expected = make_inst(END, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "GETC"
	expected = make_inst(TRAP, 0, 0, [3]Register{0, 0, 0}, 0x20, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "OUT"
	expected = make_inst(TRAP, 0, 0, [3]Register{0, 0, 0}, 0x21, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "PUTS"
	expected = make_inst(TRAP, 0, 0, [3]Register{0, 0, 0}, 0x22, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "IN"
	expected = make_inst(TRAP, 0, 0, [3]Register{0, 0, 0}, 0x23, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "PUTSP"
	expected = make_inst(TRAP, 0, 0, [3]Register{0, 0, 0}, 0x24, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

	line = "HALT"
	expected = make_inst(TRAP, 0, 0, [3]Register{0, 0, 0}, 0x25, 0, []string{}, "", "", []uint16{})
	check_decoder(t, line, expected)

}

func inst_compare(a, b instruction) bool {
	i := 0
	if len(a.data) != len(b.data) || len(a.labels) != len(b.labels) {
		return false
	}
	for i = 0; i < len(a.data); i++ {
		if a.data[i] != b.data[i] {
			return false
		}
	}
	for i = 0; i < len(a.labels); i++ {
		if a.labels[i] != b.labels[i] {
			return false
		}
	}
	return a.opp == b.opp && a.form == b.form && a.reg[0] == b.reg[0] &&
		a.reg[1] == b.reg[1] && a.reg[2] == b.reg[2] && a.num == b.num &&
		a.comment == b.comment &&
		a.labeloffset == b.labeloffset
}

func check_decoder(t *testing.T, line string, exp instruction) {
	act, _ := str2instruction(line, 1)
	if !inst_compare(act, exp) {
		t.Error(line, " should give:\n\t", exp, "\nbut gave\n\t", act)
	}
}

func Test_format_instruction(t *testing.T) {
	codeindent := ""
	for i := 0; i < CODE_INDENT; i++ {
		codeindent += " "
	}
	inst := make_inst(ADD, IMMEDIATE, 0, [3]Register{R1, R1, 0}, 4, 0, []string{}, "", "", []uint16{})
	expected := codeindent + "ADD R1, R1, #4"
	check_formatter(t, inst, expected)

	inst = make_inst(AND, REGISTER, 0, [3]Register{R1, R1, R3}, 4, 0, []string{}, "", "", []uint16{})
	expected = codeindent + "AND R1, R1, R3"
	check_formatter(t, inst, expected)

	inst = make_inst(ORIG, 0, 0, [3]Register{0, 0, 0}, 0, 0x3000, []string{}, "", "", []uint16{})
	expected = codeindent + ".ORIG 0x3000"
	check_formatter(t, inst, expected)

	inst = make_inst(ORIG, 0, 0, [3]Register{0, 0, 0}, 0, 0x3000, []string{}, "", "", []uint16{})
	expected = codeindent + ".ORIG 0x3000"
	check_formatter(t, inst, expected)
	/*
		commentindent := ""
		for i := 0; i < COMMENT_INDENT; i++ {
			commentindent += " "
		}

		inst = make_inst(NOCODE, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, "", "", []uint16{})
		expected = ""
		check_formatter(t, inst, expected)

		inst = make_inst(NOCODE, 0, 0, [3]Register{0, 0, 0}, 0, 0, []string{}, " ", "", []uint16{})
		expected = commentindent + "; "
		check_formatter(t, inst, expected)
	*/
}

func check_formatter(t *testing.T, inst instruction, exp string) {
	act := format_instruction(inst, 1)
	if act != exp {
		t.Error(inst, " should give:\n\t", exp, "\nbut gave\n\t", act)
	}
}
