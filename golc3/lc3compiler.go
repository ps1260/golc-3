// lc3compiler
package main

import (
	"fmt"
)

//First pass of assembling. This includes extracting the starting address and collecting
//the labels for a symbol table. Checks for simple errors
func firstpass(instarr []instruction) (bool, map[string]uint16, uint16) {
	symboltable := make(map[string]uint16)
	found_start, found_end, err := false, false, false
	startaddress, current_address := uint16(0), uint16(0)
	for i := range instarr {
		//.ORIG has already been give, but not END, so we should have normal instructions
		//here
		if found_start && !found_end {
			//adds the labels into the sybol table map, with the corresponding
			//address
			for j := range instarr[i].labels {
				symboltable[instarr[i].labels[j]] = current_address
			}
			//Check for some basic errors, such as multiple .ORIG and bounds checks
			//on explicit offsets. Also, increase the current_address pointer
			//by the corresponding instruction size
			switch instarr[i].opp {
			case ORIG:
				fmt.Println(i+1, "- Error: Multiple .ORIG defined")
				err = true
			case END:
				found_end = true
			case DATA:
				if len(instarr[i].data) > 0 {
					current_address += uint16(len(instarr[i].data))
				} else {
					current_address++
				}
			case BR:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 9, i+1)
				}
				current_address++
			case ADD:
				if instarr[i].form == 1 {
					int_bounds_check(&err, instarr[i].num, 5, i+1)
				}
				current_address++
			case AND:
				if instarr[i].form == 1 {
					int_bounds_check(&err, instarr[i].num, 5, i+1)
				}
				current_address++
			case JSR:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 11, i+1)
				}
				current_address++
			case LD:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 9, i+1)
				}
				current_address++
			case LDI:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 9, i+1)
				}
				current_address++
			case LDR:
				int_bounds_check(&err, instarr[i].num, 6, i+1)
				current_address++
			case LEA:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 9, i+1)
				}
				current_address++
			case ST:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 9, i+1)
				}
				current_address++
			case STI:
				if instarr[i].labeloffset == "" {
					int_bounds_check(&err, instarr[i].num, 9, i+1)
				}
				current_address++
			case STR:
				int_bounds_check(&err, instarr[i].num, 6, i+1)
				current_address++
			case NOCODE:
			default:
				current_address++
			}
			//If we have not yet found a .ORIG. If there's code before it, then
			//we have an error, but it will move into found_start mode to allow
			//for error checks for the rest of the code.
		} else if !found_start {
			switch instarr[i].opp {
			case NOCODE:
			case ORIG:
				startaddress = instarr[i].unum
				fmt.Println("start address: ", startaddress)
				current_address = startaddress
				found_start = true
			default:
				fmt.Println(i+1, "- Error: Assembly before .ORIG defined")
				found_start = true
				err = true
			}
			//This is the case for when we have already had a .END instruction
			//There should not be any assembly after .END
		} else {
			if instarr[i].opp != NOCODE {
				fmt.Println(i+1, "- Error: Assembly after .END")
				err = true
			}
		}
	}
	//Require that the assembly have a final .END statement
	if !found_end {
		fmt.Println("Error: Missing .END")
		err = true
	}
	return err, symboltable, startaddress
}

//Takes an instruction, and assembles it into the corresponding binary values.
//is_code returns false iff the instruction does not yield any binary (i.e. a line
//of comments or labels)
func compile_instruction(in instruction, symbol_table map[string]uint16, address *uint16) (out []uint16, is_code bool) {
	is_code = true
	switch in.opp {
	case ADD:
		if in.form == 0 {
			out = append(out, 0x1000+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6+uint16(in.reg[2]))
		} else {
			out = append(out, 0x1020+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6+uint16(in.num)&0x001f)
		}
		*address++
	case AND:
		if in.form == 0 {
			out = append(out, 0x5000+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6+uint16(in.reg[2]))
		} else {
			out = append(out, 0x5020+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6+uint16(in.num)&0x001f)
		}
		*address++
	case BR:
		out = append(out, switch_offset(uint16(in.num)&0x01ff, in.labeloffset, symbol_table, *address)&0x01ff+in.cc<<9)
		*address++
	case JMP:
		out = append(out, 0xc000+uint16(in.reg[0])<<6)
		*address++
	case JSR:
		out = append(out, 0x4800+uint16(in.num)&0x07ff)
		*address++
	case JSRR:
		out = append(out, 0x4000+uint16(in.reg[0])<<6)
		*address++
	case LD:
		out = append(out, 0x2000+uint16(in.reg[0])<<9+switch_offset(uint16(in.num)&0x01ff, in.labeloffset, symbol_table, *address)&0x01ff)
		*address++
	case LDI:
		out = append(out, 0xa000+uint16(in.reg[0])<<9+switch_offset(uint16(in.num)&0x01ff, in.labeloffset, symbol_table, *address)&0x01ff)
		*address++
	case LDR:
		out = append(out, 0x6000+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6+uint16(in.num)&0x003f)
		*address++
	case LEA:
		out = append(out, 0xe000+uint16(in.reg[0])<<9+switch_offset(uint16(in.num)&0x01ff, in.labeloffset, symbol_table, *address)&0x01ff)
		*address++
	case NOT:
		out = append(out, 0x903f+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6)
		*address++
	case RET:
		out = append(out, 0xc1c0)
		*address++
	case RTI:
		out = append(out, 0x8000)
		*address++
	case ST:
		out = append(out, 0x3000+uint16(in.reg[0])<<9+switch_offset(uint16(in.num)&0x01ff, in.labeloffset, symbol_table, *address)&0x01ff)
		*address++
	case STI:
		out = append(out, 0xb000+uint16(in.reg[0])<<9+switch_offset(uint16(in.num)&0x01ff, in.labeloffset, symbol_table, *address)&0x01ff)
		*address++
	case STR:
		out = append(out, 0x7000+uint16(in.reg[0])<<9+uint16(in.reg[1])<<6+uint16(in.num)&0x003f)
		*address++
	case TRAP:
		out = append(out, 0xf000+uint16(in.num)&0x00ff)
		*address++
	case DATA:
		if in.labeloffset != "" {
			out = append(out, symbol_table[in.labeloffset])
			*address++
		} else if len(in.data) > 0 {
			out = in.data
			*address += uint16(len(in.data))
		} else {
			out = []uint16{in.unum}
			*address++
		}
	default:
		is_code = false
	}
	return
}

// return an offset based on label, if label exists, explicit offset otherwise
func switch_offset(inint uint16, instring string, symbol_table map[string]uint16, address uint16) uint16 {
	if instring != "" {
		return symbol_table[instring] - address - 1
	} else {
		return inint
	}
}

//bounds check for offsets, immediates
func int_bounds_check(err *bool, offset int16, bit_size uint16, line_num int) {
	if offset >= 1<<(bit_size-1) {
		fmt.Println(line_num, "- Error: Argument too large for a", bit_size, "bit signed number.")
		*err = true
	} else if -offset > 1<<(bit_size-1) {
		fmt.Println(line_num, "- Error: Argument too small for a", bit_size, "bit signed number.")
		*err = true
	}
}
