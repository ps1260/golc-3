// binaryconverter
package main

import (
	"fmt"
	"strconv"
	"strings"
)

//Takes a string corresponding to a line of a .bin file, which may have a 16 bit
//binary value, plus comments after a ";"
func decode_binary(in string, line_num int) (out uint16, is_code, err bool) {
	is_code, err = true, false
	//Split by ";" into code and comment, and take only the code
	code := strings.TrimSpace(strings.SplitN(in, ";", 2)[0])
	//If code is empty, this is not code and we're done
	if code == "" {
		is_code = false
		return
	}
	//Remove any spaces and tabs between the 0s and 1s
	code = strings.Replace(strings.Replace(code, " ", "", -1), "\t", "", -1)
	//We must have 16 chars, if not, we have too many/few bits, or perhaps
	//someone forgot the ";" for a comment
	if len(code) != 16 {
		fmt.Println(line_num, "- Error: Wrong number of bits in binary instruction?")
		is_code = false
		err = true
		return
	}
	//Parse the binary string as a 16 bit binary number
	num, converr := strconv.ParseUint(code, 2, 16)
	//Error check the parser
	if converr != nil {
		fmt.Println(line_num, "- Error: Failed to parse binary instruction")
		is_code = false
		err = true
		return
	}
	//Now all that's left is to type cast and return the parsed value
	out = uint16(num)
	return
}
