// golc3types
package main

type Oppcode int
type Register int

//enum for opcodes
const (
	ADD Oppcode = iota
	AND
	BR
	JMP
	JSR
	JSRR
	LD
	LDI
	LDR
	LEA
	NOT
	RET
	RTI
	ST
	STI
	STR
	TRAP
	DATA
	NOCODE
	ORIG
	END
)

type FORM int

const (
	REGISTER FORM = iota
	IMMEDIATE
	LABEL
	BLKW
)

//enum for registers
const (
	R0 Register = iota
	R1
	R2
	R3
	R4
	R5
	R6
	R7
)

const (
	P uint16 = iota + 1
	Z
	ZP
	N
	NP
	NZ
	NZP
)

//structure of an operation
type instruction struct {
	opp         Oppcode
	form        FORM
	cc          uint16
	reg         [3]Register
	num         int16
	unum        uint16
	labels      []string
	comment     string
	labeloffset string
	data        []uint16
}

type lc3_system_state struct {
	ir, pc, mar, mdr, n, z, p, kbdr, kbsr, dsr, ddr, sp, psr, saved_ssp, saved_usp, priority uint16
	vector, intv                                                                             byte
	memory                                                                                   [0xffff + 1]uint16
	regfile                                                                                  [8]uint16
	interupt, ben                                                                            bool
}

func make_inst(opp Oppcode, form FORM, cc uint16, reg [3]Register, num int16, unum uint16, labels []string, comment string, labeloffset string, data []uint16) (out instruction) {
	out.opp = opp
	out.form = form
	out.cc = cc
	out.reg[0] = reg[0]
	out.reg[1] = reg[1]
	out.reg[2] = reg[2]
	out.num = num
	out.unum = unum
	out.labels = labels
	out.comment = comment
	out.labeloffset = labeloffset
	out.data = data
	return
}
