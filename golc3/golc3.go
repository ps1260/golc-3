// golc3as
package main

import (
	"fmt"
	"os"
	//"strings"
)

const load_lc3os = true

func main() {
	args := os.Args
	if len(args) == 1 {
		fmt.Println("golc3: LC-3 utilities, written in Go")
		fmt.Println("Please give the desired operation followed by the appropriate filenames.")
		fmt.Println("\tformat\n\t\tFormat the assembly into a cleaner, consistent format.")
		fmt.Println("\t\tGives some feedback, error checking.")
		fmt.Println("\tassemble\n\t\tConvert the assembly into machine code. Also produces .sym")
		fmt.Println("\t\tfile with a symbol file.")
		fmt.Println("\tconvert\n\t\tConvert binary into machine code file.")
		fmt.Println("\tsimulate\n\t\tSimulates machine code files.")
		fmt.Println("\tdebugsimulate\n\t\tSimulates machine code files, but it steps")
		fmt.Println("\t\tthrough the instructions one at a time, printing the current")
		fmt.Println("\t\tstate.")
		fmt.Println("\tdisassemble\n\t\tTakes .obj files and does a best guess conversion to the source")
		fmt.Println("\t\tassembly.")
		fmt.Println("\tdebugromdump\n\t\tLoads the .obj files and dumps the memory content.")
		fmt.Println("\tdebugparse\n\t\tDumps the internal representation of assembly code into a file.")
		return
	}
	operation := 0
	verbose := false
	switch args[1] {
	case "format":
		operation = 1
	case "assemble":
		operation = 2
	case "convert":
		operation = 3
	case "simulate":
		operation = 4
	case "debugsimulate":
		operation = 4
		verbose = true
	case "disassemble":
		operation = 5
	case "debugromdump":
		operation = 8
	case "debugparse":
		operation = 9
	default:
		operation = 0
	}
	if operation == 0 {
		fmt.Println("Error: Missing operation.")
		fmt.Println("Please give the desired operation followed by the appropriate filenames.")
		return
	}
	if len(args) < 3 {
		fmt.Println("Error: Missing input files.")
		fmt.Println("Please give the desired operation followed by the appropriate filenames.")
	}
	switch operation {
	//Format the assembly code
	case 1:
		for _, filename := range args[2:] {
			temp_err := false
			err := false
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".asm" {
				fmt.Println("Error: \"" + filename + "\" is not a .asm file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Formatting ", filename)
			//load the assembly file as an array of strings
			sourcecode := load_file_strarr(filename + ".asm")
			//parse the source code into an array of instruction data types
			instructionarr := make([]instruction, len(sourcecode))
			for i := range sourcecode {
				instructionarr[i], temp_err = str2instruction(sourcecode[i], i+1)
				err = err || temp_err
			}
			if err {
				fmt.Println("Errors in code. Exiting...")
			}
			//reformat the instruction data type into a specifically formatted string
			//and save it to a new asm file
			formatted_code := make([]string, len(sourcecode))
			for i := range sourcecode {
				formatted_code[i] = format_instruction(instructionarr[i], i+1)
			}
			write_file_strarr(filename+"_clean.asm", formatted_code)
		}
		//Assemble the code to binary
	case 2:
		for _, filename := range args[2:] {
			temp_err := false
			err := false
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".asm" {
				fmt.Println("Error: \"" + filename + "\" is not a .asm file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Assembling ", filename)
			//load the assembly file as an array of strings
			sourcecode := load_file_strarr(filename + ".asm")
			//parse the source code into an array of instruction data types
			instructionarr := make([]instruction, len(sourcecode))
			for i := range sourcecode {
				instructionarr[i], temp_err = str2instruction(sourcecode[i], i+1)
				//fmt.Println(sourcecode[i], instructionarr[i])
				err = err || temp_err
			}
			if err {
				fmt.Println("Errors when parsing code.")
			}
			//first pass of compiler, generate a symbol table
			err, symbol_table, starting_address := firstpass(instructionarr)
			if err {
				fmt.Println("Errors in first pass. Exiting...")
				return
			}
			//create the .sym file for the symbol table
			sym_file(symbol_table, starting_address, filename+".sym")
			//convert each instruction into the appropriate binary, and extract the
			//starting address of the code
			var binary []uint16
			address := starting_address
			for i := range instructionarr {
				binin, is_code := compile_instruction(instructionarr[i], symbol_table, &address)
				//fmt.Println(sourcecode[i], binin)
				if is_code {
					for j := range binin {
						binary = append(binary, binin[j])
					}
				}
			}
			//write the binary into a binary .obj file. First 16 bits correspond to the
			//starting address
			write_obj_file(filename, binary, starting_address)
		}
		//Convert the binary code into the binary .obj file
	case 3:
		for _, filename := range args[2:] {
			err := false
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".bin" {
				fmt.Println("Error: \"" + filename + "\" is not a .bin file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Converting ", filename)
			//load the binary file as an array of strings
			sourcecode := load_file_strarr(filename + ".bin")

			var binary []uint16
			for i := range sourcecode {
				binline, is_code, temp_err := decode_binary(sourcecode[i], i+1)
				err = err || temp_err
				if is_code && !err {
					binary = append(binary, binline)
				}
			}
			//write the binary into a binary .obj file. First 16 bits correspond to the
			//starting address
			if !err {
				fmt.Println("Conversion was successful")
				write_obj_file(filename, binary[1:], binary[0])
			}
		}
		//Simulate the program encoded by the given .obj files
	case 4:
		var memory [0xffff + 1]uint16
		var starting_address uint16
		sym_table := make(map[uint16]string)
		if load_lc3os {
			starting_address = load_obj_file("lc3os.obj", &memory)
			load_symbol_file(sym_table, "lc3os.sym")
		}
		for _, filename := range args[2:] {
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".obj" {
				fmt.Println("Error: \"" + filename + "\" is not a .obj file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Loading ", filename)
			//load the binary file into the memory array, and extract the starting address of the last loaded file
			starting_address = load_obj_file(filename+".obj", &memory)
			load_symbol_file(sym_table, filename+".sym")
		}
		simulate(memory, starting_address, verbose, sym_table)
		//disassemble program encoded by the given .obj files
	case 5:
		for _, filename := range args[2:] {
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".obj" {
				fmt.Println("Error: \"" + filename + "\" is not a .obj file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Loading ", filename)
			//load the binary file into the memory array, and extract the starting address of the last loaded file
			binarr, starting_address := load_obj_file_as_binarr(filename + ".obj")
			var assembly_code []string
			assembly_code = append(assembly_code, format_instruction(make_inst(ORIG, 0, 0, [3]Register{0, 0, 0}, 0, starting_address, []string{}, "Note: Assembly automatically generated by golc3 disassebler", "", []uint16{}), 1))
			for i, _ := range binarr {
				assembly_code = append(assembly_code, format_instruction(disassemble(binarr[i]), i+2))
			}
			assembly_code = append(assembly_code, format_instruction(make_inst(END, 0, 0, [3]Register{0, 0, 0}, 0, starting_address, []string{}, "", "", []uint16{}), 1))
			write_file_strarr(filename+"_disassembled.asm", assembly_code)

		}
	//load .obj into memory and dump it to a .rom file
	case 8:
		var memory [0xffff + 1]uint16
		for _, filename := range args[2:] {
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".obj" {
				fmt.Println("Error: \"" + filename + "\" is not a .obj file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Loading ", filename)
			//load the binary file into the memory array, and extract the starting address of the last loaded file
			load_obj_file(filename+".obj", &memory)
		}
		fmt.Println("Dumping memory")
		var memorydump [0xffff + 1]string
		for i := range memory {
			memorydump[i] = fmt.Sprintf("0x%4.4x\t0x%4.4x", i, memory[i])
		}
		write_file_strarr("memory.rom", memorydump[:])
	//Parse and dump the instruction array
	case 9:
		for _, filename := range args[2:] {
			temp_err := false
			err := false
			if len(filename) < 5 || filename[len(filename)-4:len(filename)] != ".asm" {
				fmt.Println("Error: \"" + filename + "\" is not a .asm file!")
				return
			} else {
				filename = filename[0 : len(filename)-4]
			}
			fmt.Println("Parsing ", filename)
			//load the assembly file as an array of strings
			sourcecode := load_file_strarr(filename + ".asm")
			//parse the source code into an array of instruction data types
			instructionarr := make([]instruction, len(sourcecode))
			for i := range sourcecode {
				instructionarr[i], temp_err = str2instruction(sourcecode[i], i+1)
				//fmt.Println(sourcecode[i], instructionarr[i])
				err = err || temp_err
			}
			if err {
				fmt.Println("Errors when parsing code.")
			}
			stringdump := make([]string, len(instructionarr))
			for i := range instructionarr {
				stringdump[i] = fmt.Sprint(instructionarr[i])
			}

			write_file_strarr(filename+".dump", stringdump)
		}
	}
}

//checks if an error, if so, panic
func panic_on_error(err error) {
	if err != nil {
		panic(err)
	}
}
