// disassemble
package main

const BIT5 = 0x0020
const BIT11 = 0x0800

func disassemble(in uint16) (out instruction) {
	switch in >> 12 {
	case 0x0:
		if nzp(in) != 0 {
			out.opp = BR
			out.form = nzp(in)
			out.num = pcoffset9(in)
		} else {
			out.opp = DATA
			out.unum = in
		}
	case 0x1:
		if in&BIT5 != 0 {
			out.opp = ADD
			out.reg[0] = dr(in)
			out.reg[1] = sr1(in)
			out.form = IMMEDIATE
			out.num = imm5(in)
		} else if 0x0018&in == 0 {
			out.opp = ADD
			out.reg[0] = dr(in)
			out.reg[1] = sr1(in)
			out.form = REGISTER
			out.reg[2] = sr2(in)
		} else {
			out.opp = DATA
			out.unum = in
		}
	case 0x5:
		if in&BIT5 != 0 {
			out.opp = AND
			out.reg[0] = dr(in)
			out.reg[1] = sr1(in)
			out.form = IMMEDIATE
			out.num = imm5(in)
		} else if 0x0018&in == 0 {
			out.opp = AND
			out.reg[0] = dr(in)
			out.reg[1] = sr1(in)
			out.form = REGISTER
			out.reg[2] = sr2(in)
		} else {
			out.opp = DATA
			out.unum = in
		}
	case 0xC:
		if 0x0E3F&in == 0 {
			if baser(in) == R7 {
				out.opp = RET
			} else {
				out.opp = JMP
				out.reg[0] = baser(in)
			}
		} else {
			out.opp = DATA
			out.unum = in
		}
	case 0x4:
		if BIT11&in == 0 {
			if 0x0E3F&in == 0 {
				out.opp = JSRR
				out.reg[0] = baser(in)
			} else {
				out.opp = DATA
				out.unum = in
			}
		} else {
			out.opp = JSR
			out.num = pcoffset11(in)
		}
	case 0x2:
		out.opp = LD
		out.reg[0] = dr(in)
		out.num = pcoffset9(in)
	case 0xA:
		out.opp = LDI
		out.reg[0] = dr(in)
		out.num = pcoffset9(in)
	case 0x6:
		out.opp = LDR
		out.reg[0] = dr(in)
		out.reg[1] = baser(in)
		out.num = offset6(in)
	case 0xE:
		out.opp = LEA
		out.reg[0] = dr(in)
		out.num = pcoffset9(in)
	case 0x9:
		if in&0x003F == 0x003F {
			out.opp = NOT
			out.reg[0] = dr(in)
			out.reg[1] = sr1(in)
		} else {
			out.opp = DATA
			out.unum = in
		}
	case 0x8:
		if in&0x0FFF == 0 {
			out.opp = RTI
		} else {
			out.opp = DATA
			out.unum = in
		}
	case 0x3:
		out.opp = ST
		out.reg[0] = dr(in)
		out.num = pcoffset9(in)
	case 0xB:
		out.opp = STI
		out.reg[0] = dr(in)
		out.num = pcoffset9(in)
	case 0x7:
		out.opp = STR
		out.reg[0] = dr(in)
		out.reg[1] = baser(in)
		out.num = offset6(in)
	case 0xF:
		if in&0x0F00 == 0 {
			out.opp = TRAP
			out.unum = trapvect8(in)
		} else {
			out.opp = DATA
			out.unum = in
		}
	default:
		out.opp = DATA
		out.unum = in
	}
	return
}

func dr(in uint16) Register {
	return Register((in & 0xE00) >> 9)
}
func nzp(in uint16) FORM {
	return FORM((in & 0xE00) >> 9)
}
func sr1(in uint16) Register {
	return Register((in & 0x1C0) >> 6)
}
func baser(in uint16) Register {
	return Register((in & 0x1C0) >> 6)
}
func sr2(in uint16) Register {
	return Register(in & 0x3)
}
func pcoffset9(in uint16) int16 {
	if in&0x1FF >= 0x100 {
		return int16(in&0x1FF + 0xFE00)
	} else {
		return int16(in & 0x1FF)
	}
}
func pcoffset11(in uint16) int16 {
	if in&0x7FF >= 0x400 {
		return int16(in&0x7FF + 0xF800)
	} else {
		return int16(in & 0x7FF)
	}
}
func offset6(in uint16) int16 {
	if in&0x3F >= 0x20 {
		return int16(in&0x3F + 0xFFC0)
	} else {
		return int16(in & 0x3F)
	}
}
func trapvect8(in uint16) uint16 {
	return in & 0xFF
}
func imm5(in uint16) int16 {
	if in&0x1F >= 0x10 {
		return int16(in&0x1F + 0xFFE0)
	} else {
		return int16(in & 0x1F)
	}
}
