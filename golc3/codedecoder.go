// codedecoder
package main

import (
	"fmt"
	"strconv"
	"strings"
)

type argtype int

const (
	TREG argtype = iota
	TNUM
	TREGNUM
	TUNUM
	TLBL
	TLBLOFF
	TMEM1
	TSTR
	TNONE
	TBLKW
	TLBLUNUM
)

type lc3reader struct {
	pos, end               int
	opp_found, args_found  bool
	arg_num, arg_num_found int
	arg                    [3]argtype
}

//Takes a string for a line of assembly and interprets it as an instruction type
func str2instruction(line string, line_num int) (out instruction, err bool) {
	out.opp = NOCODE
	reader := make_lc3reader(line)
	err = false
	if reader.end == -1 {
		return
	}
	var pos int
	var word string
	//fmt.Println(line_num, ":\n", line)
	for reader.next_word_lc3(line) {
		pos = reader.pos
		if line[pos] == ';' {
			out.comment = line[pos+1 : reader.end]
			return
		}
		word = reader.get_word(line)
		if !reader.opp_found {
			//We expect either label or opp
			//If we find an opp...
			if is_keyword(strings.ToUpper(word)) {
				reader.opp_found = true
				switch strings.ToUpper(word) {
				case "AND":
					out.opp = AND
					reader.args_found = false
					reader.arg_num = 3
					reader.arg[0] = TREG
					reader.arg[1] = TREG
					reader.arg[2] = TREGNUM
				case "ADD":
					out.opp = ADD
					reader.args_found = false
					reader.arg_num = 3
					reader.arg[0] = TREG
					reader.arg[1] = TREG
					reader.arg[2] = TREGNUM
				case "NOT":
					out.opp = NOT
					reader.args_found = false
					reader.arg_num = 2
					reader.arg[0] = TREG
					reader.arg[1] = TREG
				case "ST":
					out.opp = ST
					reader.args_found = false
					reader.arg_num = 2
					reader.arg[0] = TREG
					reader.arg[1] = TLBLOFF
				case "STR":
					out.opp = STR
					reader.args_found = false
					reader.arg_num = 3
					reader.arg[0] = TREG
					reader.arg[1] = TREG
					reader.arg[2] = TNUM
				case "STI":
					out.opp = STI
					reader.args_found = false
					reader.arg_num = 2
					reader.arg[0] = TREG
					reader.arg[1] = TLBLOFF
				case "LD":
					out.opp = LD
					reader.args_found = false
					reader.arg_num = 2
					reader.arg[0] = TREG
					reader.arg[1] = TLBLOFF
				case "LDI":
					out.opp = LDI
					reader.args_found = false
					reader.arg_num = 2
					reader.arg[0] = TREG
					reader.arg[1] = TLBLOFF
				case "LDR":
					out.opp = LDR
					reader.args_found = false
					reader.arg_num = 3
					reader.arg[0] = TREG
					reader.arg[1] = TREG
					reader.arg[2] = TNUM
				case "LEA":
					out.opp = LEA
					reader.args_found = false
					reader.arg_num = 2
					reader.arg[0] = TREG
					reader.arg[1] = TLBLOFF
				case "JMP":
					out.opp = JMP
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TREG
				case "RET":
					out.opp = RET
				case "RTI":
					out.opp = RTI
				case "JSR":
					out.opp = JSR
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "JSRR":
					out.opp = JSRR
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TREG
				case "BR":
					out.opp = BR
					out.cc = NZP
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRP":
					out.opp = BR
					out.cc = P
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRZ":
					out.opp = BR
					out.cc = Z
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRZP":
					out.opp = BR
					out.cc = ZP
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRN":
					out.opp = BR
					out.cc = N
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRNP":
					out.opp = BR
					out.cc = NP
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRNZ":
					out.opp = BR
					out.cc = NZ
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case "BRNZP":
					out.opp = BR
					out.cc = NZP
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLOFF
				case ".FILL":
					out.opp = DATA
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TLBLUNUM
				case ".STRINGZ":
					out.opp = DATA
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TSTR
				case ".ORIG":
					out.opp = ORIG
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TUNUM
				case ".END":
					out.opp = END
				case "TRAP":
					out.opp = TRAP
					reader.args_found = false
					reader.arg_num = 1
					reader.arg[0] = TUNUM
				case "GETC":
					out.opp = TRAP
					out.num = 0x20
				case "OUT":
					out.opp = TRAP
					out.num = 0x21
				case "PUTS":
					out.opp = TRAP
					out.num = 0x22
				case "IN":
					out.opp = TRAP
					out.num = 0x23
				case "PUTSP":
					out.opp = TRAP
					out.num = 0x24
				case "HALT":
					out.opp = TRAP
					out.num = 0x25
				case ".BLKW":
					out.opp = DATA
					reader.args_found = false
					reader.arg_num = 1
					out.form = BLKW
					reader.arg[0] = TBLKW
				default:
					fmt.Println("OPP not yet parsed: ", word)
				}
				//Else we have a label...
			} else {
				out.labels = append(out.labels, word)
			}
			//We have had an opp, so now we want arguments
		} else if !reader.args_found {
			//fmt.Println("Trying to parse arguments")
			//We expect an argument
			if reader.arg_num > reader.arg_num_found {
				switch reader.arg[reader.arg_num_found] {
				case TREG:
					switch reader.arg_num_found {
					case 0:
						out.reg[0] = reg_decode(&err, line_num, word)
					case 1:
						out.reg[1] = reg_decode(&err, line_num, word)
					case 2:
						out.reg[2] = reg_decode(&err, line_num, word)
					default:
						fmt.Println(line_num, "-ERROR: Too many aguments found.")
					}
				case TREGNUM:
					if is_reg(word) {
						out.reg[2] = reg_decode(&err, line_num, word)
						out.form = REGISTER
					} else {
						out.num = offset_decode(&err, line_num, word)
						out.form = IMMEDIATE
					}
				case TMEM1:
					is_label := false
					out.data, is_label = data_decode(word, false)
					if is_label {
						out.labeloffset = word
					}
				case TSTR:
					str := get_string(line)
					reader.pos += len(str)
					ascii_string, err := strconv.Unquote("\"" + str + "\"")
					panic_on_error(err)
					for _, x := range ascii_string {
						out.data = append(out.data, uint16(x))
					}
					out.data = append(out.data, 0x0000)
					reader.args_found = true
				case TLBL:
					out.labeloffset = word
				case TLBLOFF:
					if is_num(word) {
						out.num = offset_decode(&err, line_num, word)
						out.form = IMMEDIATE
					} else {
						out.labeloffset = word
						out.form = LABEL
					}
				case TLBLUNUM:
					if is_num(word) {
						out.unum = uoffset_decode(&err, line_num, word)
						out.form = IMMEDIATE
					} else {
						out.labeloffset = word
						out.form = LABEL
					}
				case TUNUM:
					out.unum = uoffset_decode(&err, line_num, word)
				case TNUM:
					out.num = offset_decode(&err, line_num, word)
				case TNONE:
					fmt.Println(line_num, "-ERROR: TNONE is not and argument type.")
				case TBLKW:
					for i := uoffset_decode(&err, line_num, word); i > 0; i-- {
						out.data = append(out.data, 0x0)
					}

				default:
					fmt.Println(line_num, "-ERROR: Argument type not implemented.")
				}
				reader.arg_num_found++
				//We should not have an argument now
			} else {
				fmt.Println(line_num, "-ERROR: Expected ", reader.arg_num, " arguments, found more.")
			}
		} else {
			fmt.Println("We should be done, why do we have: ", word)
		}
	}
	if err {
		fmt.Println(line_num, "-ERROR: Something happened.")
	}
	return
}

func make_lc3reader(in string) (out lc3reader) {
	out.pos = 0
	out.end = len(in)
	out.opp_found = false
	out.args_found = true
	out.arg_num = 0
	out.arg_num_found = 0
	out.arg[0] = TNONE
	out.arg[1] = TNONE
	out.arg[2] = TNONE
	return
}

func (in *lc3reader) read_lc3(line string) bool {
	var i int
	for i = in.pos; i < in.end && is_separator(line[i]); i++ {

	}
	in.pos = i
	return i != in.end
}

func (in *lc3reader) next_word_lc3(line string) bool {
	if in.pos > in.end {
		return false
	}
	var i int
	for i = in.pos; i < in.end && is_separator(line[i]); i++ {

	}
	in.pos = i
	return i != in.end
}

func (in *lc3reader) skip_word(line string) {
	var i int
	for i = in.pos; i < in.end && !is_separator(line[i]); i++ {

	}
	in.pos = i
	return

}

func is_separator(in byte) bool {
	return in == ' ' || in == '\t' || in == ','
}

func (in *lc3reader) get_word(line string) string {
	var j int
	i := in.pos
	for j = i; j < len(line) && !is_separator(line[j]); j++ {

	}
	in.pos = j + 1
	return line[i:j]
}

//Is the current word a LC-3 codeword? Assumes the input is all caps!!!
//The last two lines of BR*** are not proper keywords, but they may be likely
//mispellings which are dealt with in a special fashion
func is_keyword(capword string) bool {
	return capword == "AND" || capword == "ADD" || capword == "NOT" ||
		capword == "LEA" || capword == "LD" || capword == "LDR" || capword == "LDI" ||
		capword == "STI" || capword == "STR" || capword == "ST" || capword == "JSR" || capword == "JMP" ||
		capword == "BR" || capword == "RTI" || capword == "RET" ||
		capword == ".ORIG" || capword == ".END" || capword == ".FILL" || capword == ".STRINGZ" ||
		capword == "BRN" || capword == "BRNZ" || capword == "BRNZP" ||
		capword == "BRZ" || capword == "BRZP" || capword == "BRNP" || capword == "BRP" ||
		capword == "HALT" || capword == "OUT" || capword == "IN" || capword == "PUTSP" ||
		capword == "PUTS" || capword == "GETC" || capword == "TRAP" || capword == "JSRR" ||
		capword == ".BLKW"
}

//A quick check to see if the given string is (likely) a number (has to have # for
//decimal, or else be hex)
func is_num(in string) bool {
	return in[0] == 'X' || in[0] == 'x' || in[0] == '#' || in[:2] == "0X" || in[:2] == "0x"
}

//A quick check to see if the given string is (likely) a register
func is_reg(in string) bool {
	return in[0] == 'R' || in[0] == 'r'
}

//Checks the length of the line of assembly, and checks if it matches the expected
//command length. If not, give an error message
func arg_len_check(length, num_of_args, i, line_num int, err *bool) {
	if length != i+num_of_args+1 {
		fmt.Println(line_num, "- Error: Expected", num_of_args, "arguments. Detected", length-i-1)
		*err = true
	}
}
