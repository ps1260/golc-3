// golc3sim
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	//"strconv"
	"bufio"
	"time"
)

var regfile [8]uint16
var memory [65536]uint16
var ir, pc, mar, mdr, n, z, p, kbdr, kbsr, dsr, ddr, sp, psr, saved_ssp, saved_usp, priority uint16
var vector, intv byte
var ben, interupt bool

func main() {
	//User should give arguements [starting address, in hex] [filename(s) of memory files]
	if len(os.Args[1:]) == 0 {
		fmt.Println("\tGoLC-3: A LC-3 simulator written in go")
		fmt.Println("Usage:")
		fmt.Println("\tList however many .obj memory files as you need (but at least one)")
		return
	}
	//Load all the user defined memory files in the order given
	loadfile("lc3os.obj")
	filenames := os.Args[1:]
	for _, filename := range filenames {
		loadfile(filename)
	}
	//Infinite loop that evaluates an insturction, prints out LC-3 status after
	//cycle, and then waits .5 seconds til next cycle
	for {
		evaluate_instruction()
		fmt.Printf("\tPC: 0x%4.4X IR: 0x%4.4X NZP: %d%d%d\n", pc, ir, n, z, p)
		fmt.Printf("Registers: 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X 0x%4.4X\n\n",
			regfile[0], regfile[1], regfile[2], regfile[3], regfile[4], regfile[5], regfile[6], regfile[7])
		time.Sleep(500 * time.Millisecond)

		//Detect some special end of program cases to cease operation in a nice way
		if ir == 0xF025 {
			fmt.Println("Detecting a HALT instruction. Goodbye...")
			return
		} else if bit(memory[0xfffe], 15) == 1 {
			fmt.Println("The Machine control register has triggered a poweroff. Goodbye...")
			return
		}
	}
}

//Load an obj file into the LC-3 memory
func loadfile(filename string) {
	filecontents, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("bad filename")
	}
	//We read the file as bytes, but LC-3 is 16 bit, so we must join each pair of
	//bytes together
	//The first 16 bits of the file are the starting address of the contents
	address := uint16(filecontents[0])<<8 + uint16(filecontents[1])
	//sets the starting PC to the begining of the file contents
	pc = address
	for i := 2; i < len(filecontents); i += 2 {
		memory[address] = uint16(filecontents[i])<<8 + uint16(filecontents[i+1])
		address++
	}
}

//Evalutes the instruction at the given location PC. Based on the flow chart in
//the LC-3 book
func evaluate_instruction() {
	mar = pc
	pc++
	if interupt {
		interupt_sequence()
	} else {
		mdr = memory[mar]
		ir = mdr
		ben = (n == bit(ir, 11)) || (z == bit(ir, 10)) || (p == bit(ir, 9))
		decode_ir()
		switch ir >> 12 {
		case 0x1:
			add()
		case 0x5:
			and()
		case 0x0:
			br()
		case 0xc:
			jmp()
		case 0x4:
			jsr()
		case 0x2:
			ld()
		case 0xa:
			ldi()
		case 0x6:
			ldr()
		case 0xe:
			lea()
		case 0x9:
			not()
		case 0x8:
			rti()
		case 0x3:
			st()
		case 0xb:
			sti()
		case 0x7:
			str()
		case 0xf:
			trap()
		case 0xd:
			undefined_opcode()
		default:
			panic("undefined instruction")
		}
	}
}

//For debugging, decode the current ir as assembly
func decode_ir() {
	switch ir >> 12 {
	case 0x1:
		fmt.Print("ADD ")
		if bit(ir, 5) == 1 {
			fmt.Printf("R%d, R%d, #%d\n", dr(), sr1(), ustos(sext(imm5(), 5)))
		} else {
			fmt.Printf("R%d, R%d, R%d\n", dr(), sr1(), sr2())
		}
	case 0x5:
		fmt.Print("AND ")
		if bit(ir, 5) == 1 {
			fmt.Printf("R%d, R%d, #%d\n", dr(), sr1(), ustos(sext(imm5(), 5)))
		} else {
			fmt.Printf("R%d, R%d, R%d\n", dr(), sr1(), sr2())
		}
	case 0x0:
		fmt.Print("BR")
		switch dr() {
		case 1:
			fmt.Print("p")
		case 2:
			fmt.Print("z")
		case 3:
			fmt.Print("zp")
		case 4:
			fmt.Print("n")
		case 5:
			fmt.Print("np")
		case 6:
			fmt.Print("nz")
		case 7:
			fmt.Print("nzp")
		default:
		}
		fmt.Print(" %4.4x\n", pc+sext(pcoffset9(), 9))
	case 0xc:
		fmt.Print("JMP ")
	case 0x4:
		fmt.Print("JSR ")
	case 0x2:
		fmt.Print("LD ")
	case 0xa:
		fmt.Print("LDI ")
	case 0x6:
		fmt.Print("LDR ")
	case 0xe:
		fmt.Print("LEA ")
	case 0x9:
		fmt.Printf("NOT R%d, R%d\n", dr(), sr1())
	case 0x8:
		fmt.Print("RTI ")
	case 0x3:
		fmt.Print("ST ")
	case 0xb:
		fmt.Print("STI ")
	case 0x7:
		fmt.Print("STR ")
	case 0xf:
		fmt.Print("TRAP ")
	case 0xd:
		fmt.Println("DATA: %4.4x", ir)
		return
	default:
		panic("undefined instruction")
	}
}

//unsigned to signed
func ustos(num uint16) int16 {
	if bit(num, 15) == 1 {
		return -1 - int16(^num)
	} else {
		return int16(num)
	}
}

//Returns the i-th bit of num as a boole
func bit(num uint16, i uint16) uint16 {
	return (num & (1 << i)) >> i
}

//sign extends the number num, where bitlen is the size of unextended
//number
func sext(num, bitlen uint16) uint16 {
	if bit(num, bitlen-1) == 1 {
		return num + 0xffff<<bitlen
	} else {
		return num
	}
}

//sets the conditional codes n,z,p based on the sign of the current destination
//register
func setcc() {
	if regfile[dr()] == 0 {
		n = 0
		z = 1
		p = 0
	} else if bit(regfile[dr()], 15) == 1 {
		n = 1
		z = 0
		p = 0
	} else {
		n = 0
		z = 0
		p = 1
	}
}

/*
The next functions read the various parts of the instruction register, and
return them as uint16
*/

//gives the value of the base register
func baser() uint16 {
	return (ir & 0x01c0) >> 6
}

//gives the value of the destination register
func dr() uint16 {
	return (ir & 0x0e00) >> 9
}

//gives the source register (#1) of the current instruction
func sr1() uint16 {
	return (ir & 0x01c0) >> 6
}

//gives the source register (#2) of the current instruction
func sr2() uint16 {
	return (ir & 0x0007)
}

//gives the immediate 5 bit value
func imm5() uint16 {
	return (ir & 0x001f)
}

//gives the 6 bit offset value
func offset6() uint16 {
	return (ir & 0x003f)
}

//gives the 9 bit pc offset
func pcoffset9() uint16 {
	return (ir & 0x01ff)
}

//gives the 11 bit pc offset
func pcoffset11() uint16 {
	return (ir & 0x07ff)
}

//these read/write functions allow for memory mapping user i/o
func memread() {
	switch mar {
	case 0xfe02:
		//gets input from the user. Not ideal, since it requires an enter to trigger
		r := bufio.NewReader(os.Stdin)
		kbdr, err := r.ReadByte()
		if err != nil {
			panic(err)
		}
		mdr = uint16(kbdr)
	case 0xfe00:
		//kbsr is always triggered (kbsr[15]=1) since the read will wait until user
		//input. If a better method is found, then this should be changed
		kbsr = 0x8000
		mdr = kbsr
	case 0xfe04:
		//dsr is always triggered (dsr[15]=1) since the display here is always
		//ready to print
		dsr = 0x8000
		mdr = dsr
	default:

		mdr = memory[mar]
	}
}

func memwrite() {
	fmt.Println("Woot, a write op")
	if mar == 0xfe06 {
		ddr = mdr
		fmt.Printf("%c", uint8(ddr))
	} else {
		memory[mar] = mdr
	}
}

/*
Next up we have the various opcode. These are based on the flow chart from the book.
Each one corresponds to a single opcode, and if the opcode coresponds to multiple
instructions, it will interpret it accordingly
*/

func add() {
	if bit(ir, 5) == 1 {
		regfile[dr()] = regfile[sr1()] + sext(imm5(), 5)
	} else {
		regfile[dr()] = regfile[sr1()] + regfile[sr2()]
	}
	setcc()
}
func and() {
	if bit(ir, 5) == 1 {
		regfile[dr()] = regfile[sr1()] & sext(imm5(), 5)
	} else {
		regfile[dr()] = regfile[sr1()] & regfile[sr2()]
	}
	setcc()
}
func br() {
	if ben {
		pc = pc + sext(pcoffset9(), 9)
	}
}
func jmp() {
	pc = regfile[baser()]
}
func jsr() {
	regfile[7] = pc
	if bit(ir, 11) == 1 {
		pc = pc + sext(pcoffset11(), 11)
	} else {
		pc = regfile[baser()]
	}
}
func ld() {
	mar = pc + sext(pcoffset9(), 9)
	memread()
	regfile[dr()] = mdr
	setcc()
}
func ldi() {
	mar = pc + sext(pcoffset9(), 9)
	memread()
	mar = mdr
	memread()
	regfile[dr()] = mdr
	setcc()
}
func ldr() {
	mar = baser() + sext(offset6(), 6)
	memread()
	regfile[dr()] = mdr
	setcc()
}
func lea() {
	regfile[dr()] = pc + sext(pcoffset9(), 9)
	setcc()
}
func not() {
	regfile[dr()] = ^sr1()
	setcc()
}
func rti() {
	panic("ERROR: Attempt to execute RTI. This is unknown/unfinished territory. Exiting...")
	mar = sp
	if bit(psr, 15) == 1 {
		vector = 0x00
		mdr = psr
		psr = psr & 0x7fff
		line_45()
	} else {
		memread()
		pc = mdr
		mar = sp + 1
		sp++
		memread()
		psr = mdr
		sp++
		if bit(psr, 15) == 1 {
			saved_ssp = sp
			sp = saved_usp
		}
	}
}
func st() {
	mar = pc + sext(pcoffset9(), 9)
	mdr = regfile[sr1()]
	memory[mar] = mdr
}
func sti() {
	mar = pc + sext(pcoffset9(), 9)
	memread()
	mar = mdr
	mdr = regfile[sr1()]
	memory[mar] = mdr
}
func str() {
	mar = baser() + sext(offset6(), 6)
	mdr = regfile[sr1()]
	memory[mar] = mdr
}
func trap() {
	mar = ir & 0x00ff
	memread()
	regfile[7] = pc
	pc = mdr
}
func undefined_opcode() {
	panic("ERROR: Attempted to execute undefined opcode. Exiting...")
	vector = 0x01
	mdr = psr
	psr = psr & 0x7fff
	if bit(psr, 15) == 1 {
		line_45()
	} else {
		line_37()
	}
}
func line_45() {
	saved_usp = sp
	sp = saved_ssp
	line_37()
}
func line_37() {
	mar = sp - 1
	sp = sp - 1
	memwrite()
	mdr = pc + 1
	mar = sp - 1
	sp = sp - 1
	memwrite()
	mar = 0x0100 + uint16(vector)
	memread()
	pc = mdr
}

//This is wonky, not fully implemented stuff
func interupt_sequence() {
	vector = intv
	psr = psr & (0xf8ff + priority<<8)
	mdr = psr
	psr = psr & 0x7fff
	if bit(psr, 15) == 1 {
		line_45()
	} else {
		line_37()
	}
}
