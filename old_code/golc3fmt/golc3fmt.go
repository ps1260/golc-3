// golc3fmt
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const CODEINDENT int = 12
const COMMENTINDENT int = 36
const MAXLINELENGTH int = 80

func main() {
	args := os.Args
	if len(args) == 1 {
		fmt.Println("golc3fmt: A LC-3 assembly formatter, written in Go")
		fmt.Println("Please give source .asm files as arguments")
		return
	}
	for _, filename := range args[1:] {
		fmt.Println("processing ", filename)
		sourcecode := loadasmfile(filename)
		cleansourcecode := cleansource(sourcecode)
		writeasmfile(cleansourcecode, filename)
	}
}

func loadasmfile(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		panic("Bad filename give")
	}
	defer file.Close()
	var sourcecode []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sourcecode = append(sourcecode, scanner.Text())
	}
	return sourcecode
}

// writeLines writes the lines to the given file.
func writeasmfile(lines []string, filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, line := range lines {
		fmt.Fprintln(w, line)
	}
	return w.Flush()
}

func cleansource(oldcode []string) []string {
	var newcode, wordlist []string
	var templen int
	iscomment := false
	iscode := false
	for linenum, line := range oldcode {
		iscomment = false
		newline := strings.TrimSpace(line)
		//force empty lines to start with ";"
		if newline == "" {
			newcode = append(newcode, ";")
		} else if newline[0] == ';' {
			if len(newline) == 1 {
				newcode = append(newcode, newline)
			} else if newline[1] == ' ' {
				newcode = append(newcode, newline)
			} else {
				//force pure comment lines to start with "; "
				newline = "; " + newline[1:]
				newcode = append(newcode, newline)
			}
		} else {
			//ensure that comments are delimited by a " ;" character
			newline = strings.Replace(newline, ";", " ;", -1)
			//and that commas have spaces after them
			newline = strings.Replace(newline, ",", ", ", -1)
			wordlist = strings.Split(newline, " ")
			for i, _ := range wordlist {
				wordlist[i] = strings.TrimSpace(wordlist[i])
			}
			newline = ""
			for i, _ := range wordlist {
				//always just add a comment
				if iscomment {
					newline += wordlist[i] + " "
				} else
				//ignore empties
				if len(wordlist[i]) == 0 {
				} else
				//starting a comment
				if wordlist[i][0] == ';' {
					iscomment = true
					iscode = false
					templen = len(newline)
					if templen <= COMMENTINDENT {
						for i := 0; i < COMMENTINDENT-templen; i++ {
							newline += " "
						}
					} else {
						fmt.Println("Warning: Long line of assembly. Please consider refactoring line #", linenum+1)
					}
					if len(wordlist[i]) == 1 {
						newline += "; "
					} else if wordlist[i][1] != ' ' {
						newline += "; " + wordlist[i][1:]
					} else {
						newline += wordlist[i]
					}
				} else
				//is a label being declared
				if !is_keyword(wordlist[i]) && !iscode {
					newline += strings.ToUpper(wordlist[i]) + " "
				} else
				// if it is part of the instruction
				{
					if !iscode {
						iscode = true
						templen = len(newline)
						if templen <= CODEINDENT {
							for i := 0; i < CODEINDENT-templen; i++ {
								newline += " "
							}
						} else {
							fmt.Println("Warning: Long line of labels. Please consider refactoring line #", linenum+1)
							fmt.Println("\t", line)
						}
					}
					newline += strings.ToUpper(wordlist[i]) + " "
				}
			}
			if len(newline) > MAXLINELENGTH {
				fmt.Println("Warning: Very long line of source code. Please consider refactoring line #", linenum+1)
				fmt.Println("\t", line)

			}
			newcode = append(newcode, newline)
			iscode = false
			iscomment = false
		}
	}
	return newcode
}

//Is the current word a LC-3 codeword
func is_keyword(word string) bool {
	capword := strings.ToUpper(word)
	return capword == "AND" || capword == "ADD" || capword == "NOT" ||
		capword == "LEA" || capword == "LD" || capword == "LDR" || capword == "LDI" ||
		capword == "STI" || capword == "STR" || capword == "ST" || capword == "JSR" || capword == "JMP" ||
		capword == "BR" || capword == "RTI" || capword == ".ORIG" || capword == ".END" || capword == "HALT" || capword == "OUT" ||
		capword == "PUTS" || capword == ".FILL" || capword == ".STRINGZ" || capword == "TRAP" || capword == "BRN" ||
		capword == "BRNZ" || capword == "BRNZP" || capword == "BRZ" || capword == "BRZP" || capword == "BRNP" ||
		capword == "R0" || capword == "R1" || capword == "R2" || capword == "R3" ||
		capword == "R4" || capword == "R5" || capword == "R6" || capword == "R7"

}
