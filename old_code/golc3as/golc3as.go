// golc3as
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var symboltable map[string]uint16
var initialaddress uint16
var sourcecode []string

func main() {
	args := os.Args
	if len(args) == 1 {
		fmt.Println("golc3as: A LC-3 assembler, written in Go")
		fmt.Println("Please give source .asm files as arguments")
		return
	}
	for _, filename := range args[1:] {
		fmt.Println("processing ", filename)
		loadasmfile(filename)
		symboltable = make(map[string]uint16)
		firstpass()
	}
	initialaddress = 0xffff
	fmt.Println("Hello World! ", initialaddress)
}

func loadasmfile(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		panic("Bad filename give")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sourcecode = append(sourcecode, scanner.Text())
	}
	return
}

//First pass cleans up the code a bit. Will make the symbol table mapping each
//label to a memory location
func firstpass() {
	//var label string
	var address uint16
	is_origin_set := false
	is_label := true

	for i := range sourcecode {
		//only on non comment lines
		if sourcecode[i][0] != ';' {
			wordarr := strings.Split(strings.Replace(sourcecode[i], ",", "", -1), " ")
			for j := 0; j < len(wordarr) && is_label; j++ {
				if is_keyword(wordarr[j]) {
					switch strings.ToUpper(wordarr[j]) {
					case ".ORIG":
						if is_origin_set {
							fmt.Println("ERROR: Multiple .ORIG given")
							return
						} else {
							num, err := strconv.ParseUint(wordarr[j+1], 0, 16)
							errorpanic(err)
							initialaddress = uint16(num)
							address = initialaddress
						}
					case ".STRINGZ":
						address += uint16(len(wordarr[j+1]))
					case ".END":
						return
					default:
						address++
					}
					is_label = false
				} else {
					symboltable[wordarr[j]] = address
				}
			}
		}
	}
}

//Is the current word a LC-3 codeword
func is_keyword(word string) bool {
	capword := strings.ToUpper(word)
	return capword == "AND" || capword == "ADD" || capword == "NOT" ||
		capword == "LEA" || capword == "LD" || capword == "LDR" || capword == "LDI" ||
		capword == "STI" || capword == "STR" || capword == "ST" || capword == "JSR" || capword == "JMP" ||
		capword == "BR" || capword == "RTI" || capword == ".ORIG" || capword == ".END" || capword == "HALT" || capword == "OUT" ||
		capword == "PUTS" || capword == ".FILL" || capword == ".STRINGZ" || capword == "TRAP" || capword == "BRN" ||
		capword == "BRNZ" || capword == "BRNZP" || capword == "BRZ" || capword == "BRZP" || capword == "BRNP" ||
		capword == "R0" || capword == "R1" || capword == "R2" || capword == "R3" ||
		capword == "R4" || capword == "R5" || capword == "R6" || capword == "R7"
}

func errorpanic(err error) {
	if err != nil {
		panic(err)
	}
}
