# golc-3
LC-3 tools written in Go
 
## Overview
[LC-3](https://en.wikipedia.org/wiki/LC-3) is an educational ISA described in the Patt and Patel book *Introduction to Computing Systems: From Bits and Gates to C and Beyond*. This book and the LC-3 language form a foundation for introductory computer engineering classes such as ECE 120 at UIUC.
 
This is just a fun side project. It's a product of boredom with no concrete goals and is likely a bad example of code. It is based on the description of LC-3 from the Patt and Patel book, and aims to have some degree of functionality, similiar to that of the original [LC-3 tools (written in C)](https://highered.mheducation.com/sites/0072467509/student_view0/lc-3_simulator.html).

Originally, this project only tried to implement a simulator, but has since grow to attempt compilation (or rather assembling) and assembly formatting. Initial work at such functionality in the various programs in old_code/, but now the functionality of those smaller programs is rolled into one program in golc3.

## golc3/golc3
Current Functionality (some limitations apply, see next section):
* Takes argument "assemble", "format", "convert", or "simulate", followed by the filepaths to the corresponding file(s)
* Parse the source code into an internal format that holds each lines labels, instruction, and comments
* Assemble assembly into binary (some error checks are done, not all things work properly, but the basic, more common instructions should) and generate the symbol table file
* Format assembly formatted to a particular convention (with some formatting feedback)
* Convert a .bin binary code file into the .obj file. This properly works 100% (probably only thing to do so at the moment) 
* Simulate a bunch of .obj files (starting address of the last given file will be used). Simulation limitations are (currently and more-or-less) the same as that of the original (old_code/golc3sim/golc3sim, explained below) code. For step by step printing of machine state, use "debugsimulate".
* Additional debugging arguements "debugsimulate" (old behavior of "simulate", current "simulate" doesn't print state), "debugromdump" (loads multiple .obj's and makes a human readable memory.rom file with the contents of the loaded memory), and "debugparse" (dumps the parsed assembly code. Used only for development) 
 
Missing/Different Functionality, TODO:
* Is not fully compliant with the original LC-3 tools (i.e. this does not require commas between arguements, either commas or spaces). May or may not seek full compliance
* Not all arguments are checked
* Simulator does not yet look for lc3os.obj
* Much, much, more (probably)

## old_code/golc3sim/golc3sim
Current Functionality:
* Load LC-3 binary files into memory (it will automatically search for, and load, a lc3os.obj file that contains the LC-3 OS. This can be found in the source code for the original LC-3 simulator) which are given a arguements.
* Begin execution at the starting address of the last .obj file loaded.
* Execute instructions step-by-step, and output the current state of the registers
* Decode the currently executed instruction (or attempt to) and display its assembly
* It should end execution when it detects a machine shut down, or HALT instruction

Missing Functionality:
* Interupts are not really implemented
* User IO (lacks raw user input, not sure if output works)
* Proper user interaction (choosing to step through, or simply execute without debugging information)
